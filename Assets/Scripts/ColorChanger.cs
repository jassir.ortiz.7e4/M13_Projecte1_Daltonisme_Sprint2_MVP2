using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{

    [SerializeField] private int range = 0;
    private int randomGris;
    SpriteRenderer m_SpriteRenderer;

    public void setColorHex(string colorHex) {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();


        if (colorHex == "dd44b0")
        {
            randomGris = UnityEngine.Random.Range(50, 170);
            Color32 c = new Color32((byte)((byte)0+randomGris), (byte)(0 + randomGris), (byte)(0 + randomGris), 255);
            m_SpriteRenderer.color = c;


        }
        else {
            byte r = (byte)Convert.ToInt32(colorHex.Substring(0, 2), 16);
            byte b = (byte)Convert.ToInt32(colorHex.Substring(2, 2), 16);
            byte g = (byte)Convert.ToInt32(colorHex.Substring(4, 2), 16);


            Color32 c = new Color32((byte)UnityEngine.Random.Range(minValue(r), maxValue(r)), (byte)UnityEngine.Random.Range(minValue(b), maxValue(b)), (byte)UnityEngine.Random.Range(minValue(g), maxValue(g)), 255);
            m_SpriteRenderer.color = c;
        }        
    }

    private int minValue(int value) {

        if (value-range < 0)
        {
            return 0;


        }

        return value-range;
    
    }

    private int maxValue(int value)
    {

        if (value + range > 255)
        {
            return 255;


        }

        return value+range;

    }

    public void colorTarget(string colorHex) {
        

        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        float progress = GameManager.Instance.progress;
        byte r = (byte)Convert.ToInt32(colorHex.Substring(0, 2), 16);
        byte b = (byte)Convert.ToInt32(colorHex.Substring(2, 2), 16);
        byte g = (byte)Convert.ToInt32(colorHex.Substring(4, 2), 16);

        
        Color32 c = new Color32((byte)(r-progress), b , g , 255);
        

        m_SpriteRenderer.color = c;
    }

}
