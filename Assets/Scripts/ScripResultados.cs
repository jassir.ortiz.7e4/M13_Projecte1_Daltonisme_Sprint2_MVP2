using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScripResultados : MonoBehaviour
{
    public Text textResult;
    public Text textDaltonism;
    public Button exitButton;
    int[] countFails = GameManager.Instance.countFails;
    public string diagnostic;
    // Start is called before the first frame update
    void Start()
    {
        Diagnostic();
        exitButton = GameObject.Find("Button").GetComponent<Button>();
        exitButton.onClick.AddListener(ButtonExit);
        textResult.GetComponent<Text>();
        textResult.text = "Final score: " + GameManager.Instance.score;
        textDaltonism.text = "Type of daltonism: " + diagnostic;

    }

    void Diagnostic() {
        if (countFails[0] >= 2)
        {
            diagnostic = "MONOCROMATICO";
          
        }
        else if (countFails[1] >= 2)
        {
            diagnostic = "TRITANOPIA";

        }
        else if (countFails[2] >= 2)
        {
            diagnostic = "DEUTERANOPIA";

        }else if (countFails[3] >= 2)
        {
            diagnostic = "PROTANOPIA";

        }
        else {
            diagnostic = "NICE";
        }
    }

    void ButtonExit()
    {

        Application.Quit();
    }
}
