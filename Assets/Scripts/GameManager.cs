using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance;

    public static GameManager Instance
    {

        get
        {
            return _instance;
        }
    }

    [SerializeField] private int width = 4;
    [SerializeField] private int height = 4;
    [SerializeField] private GameObject nodePrefab;
    [SerializeField] private string colorHex;
    [SerializeField] private int targetSize;
    public Text textScore;
    public Text textIntentos;
    public Text textProgress;
    private int[] arrayRandom;
    public float progress;
    public int numEscena;
    public int score;
    public int intentos;
    
    private void Awake()
    {
        if(_instance != null && _instance != this){
            Destroy(this.gameObject);
        }
        _instance = this;

        DontDestroyOnLoad(this.gameObject);
        score = 0;
        buttonExit = GameObject.Find("ButtonExit").GetComponent<Button>();
        buttonHelp = GameObject.Find("ButtonHelp").GetComponent<Button>();
        colorHex = "dd44b0";
        Debug.Log("Stage 0 'Black'");
        Create();
        progress = 0;
        numEscena = 0;
        intentos = 2;
    }

    void Start()
    {
        buttonExit.onClick.AddListener(ButtonExit);
        buttonHelp.onClick.AddListener(ButtonHelp);
        textScore = GameObject.Find("TextScore").GetComponent<Text>();
        textIntentos = GameObject.Find("TextIntent").GetComponent<Text>();
        textProgress = GameObject.Find("TextProgress").GetComponent<Text>();
    }


    private void Update()
    {
        textScore.text = "Score: " + score;
        textIntentos.text = "attempts: " + intentos;
        textProgress.text = numEscena*100 / 15 + "%";
    }

    void Create()
    {
        arrayRandom = randomGrid();
        GeneratoGrid();
        createTarget();
    }

    int[] randomGrid() {
        arrayRandom = new int[2];
        arrayRandom[0] = Random.Range(0, width - targetSize + 1);
        arrayRandom[1] = Random.Range(0, height - targetSize + 1);

        return arrayRandom;
    }

    void GeneratoGrid()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                var node = Instantiate(nodePrefab, new Vector2(x, y), Quaternion.identity);
                node.GetComponent<ColorChanger>().setColorHex(colorHex);

            }
        }
    }

    

    void createTarget() {

        GameObject[] nodeList;
        nodeList = GameObject.FindGameObjectsWithTag("Node");

        for (int i = arrayRandom[0]; i < targetSize+arrayRandom[0]; i++) {

            for (int j = arrayRandom[1]; j < targetSize + arrayRandom[1]; j++) {

                for (int x = 0; x < nodeList.Length; x++ )
                {
                    if (nodeList[x].transform.position.x == i && nodeList[x].transform.position.y == j ) {                       
                        nodeList[x].GetComponent<ColorChanger>().colorTarget(colorHex);
                        nodeList[x].transform.gameObject.tag = "TargetNode";
                    }
                }
            }            
        }        
    }

    public void changeTarget()
    {

        progress += 15;
        ChangeScene();

       
    }


    public Button buttonHelp;
    public Button buttonExit;
    void ButtonExit() {
        Application.Quit();
    }

   

    public int[] countFails = new int[4];
    public void ButtonHelp() {

        if (numEscena < 3) {
            Debug.Log(numEscena);
            countFails[0] += 1;
            intentos--;

            ChangeScene();
        } else if (numEscena > 3 && numEscena < 7)
        {
            countFails[1] += 1;
            intentos--;
            ChangeScene();

        } else if (numEscena > 7 && numEscena < 11)
        {
            countFails[2] += 1;
            intentos--;
            ChangeScene();

        } else if (numEscena > 11 && numEscena < 15)
        {
            countFails[3] += 1;
            intentos--;
            ChangeScene();
        }
    }

    void ChangeScene() {
        numEscena += 1;

        Create();

        if (intentos > 0)
        {
            switch (numEscena)
            {
                case 3:
                    intentos = 2;
                    colorHex = "1482FF";
                    Debug.Log("Stage 1 'Blue'");
                    break;
                case 7:
                    intentos = 2;
                    colorHex = "32CD32";
                    Debug.Log("Stage 2 'Green'");
                    break;
                case 11:
                    intentos = 2;
                    colorHex = "FF4500";
                    Debug.Log("Stage 4 'red'");
                    break;
                case 15:
                    SceneManager.LoadScene("PantallaResultados");
                    break;
            }
        }
        else
        {
            SceneManager.LoadScene("PantallaResultados");
        }
    }
}

