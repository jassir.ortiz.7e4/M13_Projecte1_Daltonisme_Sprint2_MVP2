using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InitialScreen : MonoBehaviour
{
    public Button buttonPlay;
    public Button buttonExitGame;

    // Start is called before the first frame update
    void Start()
    {
        buttonPlay.onClick.AddListener(ButtonPlay);
        buttonExitGame.onClick.AddListener(ButtonExitGame);
    }

    void ButtonPlay() {
        SceneManager.LoadScene("PantallaJuego");
    }

    void ButtonExitGame() {
        Application.Quit();
    }
    
}
