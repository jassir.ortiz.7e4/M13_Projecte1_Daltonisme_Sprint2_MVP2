using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeCollider : MonoBehaviour
{
    private void OnMouseDown()
    {
        if (gameObject.CompareTag("TargetNode"))
        {
            GameManager.Instance.score++;
            GameManager.Instance.changeTarget();
        }
        if (!gameObject.CompareTag("TargetNode")) {
            GameManager.Instance.ButtonHelp();
        }
    }
}
